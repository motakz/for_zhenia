
require 'capybara'
require 'capybara/cucumber'
require 'rspec/expectations'
require 'capybara/dsl'
require 'selenium-webdriver'


Capybara.configure do |capybara|
    capybara.register_driver :selenium_ff do |app|
      capybara::Selenium::Driver.new(app, :browser => :firefox)
    end

  capybara.default_driver = :selenium_ff #the browser for running tests on
  capybara.run_server = false

end

