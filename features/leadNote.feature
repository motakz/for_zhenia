
Feature: As a consumer, I want to Create a new Lead and new note

  Scenario Outline: Create a new Lead and new note
    Given I'm on "<login>" page
    When I fill in "<email>" and "<password>" field
    Then I login

    Given Leads page is open
    When Click New_lead button
    Then Create lead with "<new_lead>" name

    Given Lead_detail page is open
    When Add "<new_note>" for that Lead
    Then Note shows up on the list

    Examples:
      | login | email | password | new_lead | new_note |
      | https://core.futuresimple.com/sales/users/login | zhenia.mudrak@gmail.com | rfxtxrf-7-7-open | Hull | Relax |