Given(/^I'm on "(.*?)" page$/) do |login|
  visit login
end

When(/^I fill in "(.*?)" and "(.*?)" field$/) do |email, password|
  fill_in 'user[email]', with: email
  fill_in 'user[password]', with: password
end

Then(/^I login$/) do
  find(:xpath,".//button[contains(.,'Log in')]").click
end

Given(/^Leads page is open$/) do
  find(:xpath,".//a[@id='nav-leads']").click
end

When(/^Click New_lead button$/) do
  find(:xpath,".//a[@id='leads-new']").click
end

Then(/^Create lead with "(.*?)" name$/) do |new_lead|
  fill_in 'last_name', with: new_lead
  find(:xpath,".//button[contains(.,'Save')]").click
  @lead = new_lead
end

Given(/^Lead_detail page is open$/) do
  click_link(@lead)
end

When(/^Add "(.*?)" for that Lead$/) do |new_note|
  fill_in 'note', with: new_note
  find(:xpath,".//button[contains(.,'Save')]").click
  @note = new_note
end

Then(/^Note shows up on the list$/) do
  page.find("#nav-dashboard").click
  expect(page).to have_content(@note)
end
